* Queries

Without lookup in ``snmp.yml``.

```promql
(irate(ifHCInOctets{job='CISCO_IOS-SNMP',instance=~"spine01"}[5m]) * ignoring(ifDescr) group_left(ifDescr)  ifDescr != 0) * 8
```

With lookup in ``snmp.yml``.

```promql
(irate(ifHCInOctets{instance=~"^$instance"}[5m]) != 0) * 8
```

* Variables 

```
$instance	label_values(instance)
```
