# #In/Out
```promql
(irate(ifHCInOctets{job='EXTREME_VSP-SNMP',instance=~"^$instance"}[5m]) != 0) * 8
```

## Nb VLANs
```promql
count(ifSpeed{instance="$instance", ifDescr=~"VLAN.*"})
```

## Discard
```promql
(irate(ifInDiscards{job='EXTREME_VSP-SNMP',instance=~"^$instance"}[5m]) != 0)
```

## Multicast
```promql
(irate(ifInNUcastPkts{job='EXTREME_VSP-SNMP',instance=~"^$instance"}[5m]) != 0)
(irate(ifOutNUcastPkts{job='EXTREME_VSP-SNMP',instance=~"^$instance"}[5m]) != 0)
```

## Ping latency
```promql
probe_icmp_duration_seconds{phase="rtt", instance="$instance"}
```
