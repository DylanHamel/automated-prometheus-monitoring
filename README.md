# Automate your Prometheus Configuration

###### Dylan Hamel - <dylan.hamel@protonmail.com> - Novembre 2019



The goal of this repo is to automate Protetheus configuration for using "snmp_exporter" (SNMP) and "blackbox_exporter" (ICMP)



## Setup infrastructure

```shell
docker-compose up -d
```

Verify that all containers are up

```shell
» docker ps --format "{{.Image}}: {{.Status}} :{{.Ports}}"

grafana/grafana: Restarting (1) 1 second ago :
prom/snmp-exporter:master: Up About an hour :0.0.0.0:9116->9116/tcp
prom/blackbox-exporter:master: Up 2 hours :0.0.0.0:9115->9115/tcp
prom/prometheus: Up 2 hours :0.0.0.0:9090->9090/tcp
```



## Define your parameter 

#### Hosts

Hosts that you want monitor with SNMP and/or ICMP have to be defined in `data_vars/hosts.yml` :

```yaml
hosts:
  extreme_vsp:
    - name: leaf01
    - name: leaf02
    - name: leaf03
    - name: leaf04
      ip: 10.0.5.204

  cisco_ios:
    - name: spine01
    - name: spine02
```

Hosts that you want monitor with `node_exporter` agent have to be defined in `data_vars/hosts.yml` :

```yaml
nodes_exporter:
  - dns: linux01
    ip: 10.1.1.1
    port: 9100
  - dns: linux01
    ip: 10.1.1.2
  - ip: 10.1.1.3
```



#### Credentials

SNMP credentials have to be defined in `data_vars/credentials.yml`. For security reasons this file is encrypted with `ansible-vault`.

```yaml
credentials:

  max_repetitions: 25
  retries: 3
  timeout: 30s

  extreme_vsp:
    snmp_version: 2
    community: my-community-vsp

  cisco_ios:
    snmp_version: 3
    username: user
    security_level: noAuthNoPriv
    password: pass
    auth_protocol: MD5
    priv_protocol: DES
    priv_password: otherPass
    context_name: context
```

Each credential is defined for a module (`extreme_vsp` is a module). In the `data_vars/hosts.yml`, hosts have to be defined with the right module. If not, thw wrong SNMP credentials will be used and the polling will fail !

```shell
# IN data_vars/credentials.yml
hosts:
  extreme_vsp:
  
  ### IS LINKED FOR CREDENTIALS TO 
  
# IN data_vars/hosts.yml
credentials:
  extreme_vsp:
    snmp_version: 2
    community: my-community-vsp
```



#### OIDS

With the same logic that hosts, The OIDS retrieve is linked to a module. You can define wich OID will be retrieve for each module. You can retrieve 1000 informations for a subset of devices and 1 information for an other subset.

```yaml
oids:
  _common_mib:
    - descr: ifOperStatus
      oid: 1.3.6.1.2.1.2.2.1.8

  extreme_vsp:
```

OIDs defined in ``_common_mib`` will be retrieve in every modules !!



#### Infrastructure informations

If you want you can run each module on a different server. In this case you can describe your infrastrucutre in `data_vars/infra.yml`.

```yaml
infra:
  prometheus:
    dns: localhost
    ip: 127.0.0.1
    port: 9090

  snmp_exporter:
    dns: localhost
    ip: 127.0.0.1
    port: 9116

  blackbox_exporter:
    dns: localhost
    ip: 127.0.0.1
    port: 9115
```

Each service run on a specific server. Each service has to be defined in thie file. The best way is let the services configurations as defined.

```yaml
---
services:
  snmp:
    path: /snmp
    run_on: snmp_exporter
  icmp:
    path: /probe
    run_on: blackbox_exporter
    mod: icmp_core
  node:
    path: /metrics
```



## Run the playbook

```shell
ansible-playbook run_monitoring.yml --vault-password-file=.ansible_vault_key
```



#### Ansible Vault

To see content of `data_vars/credentials.yml` use `ansible-vault`:

```shell
» ansible-vault view data_vars/credentials.yml
Vault password: # Enter password contains in .ansible_vault_pass.txt
```

To edit content of `data_vars/credentials.yml` use `ansible-vault`:

```shell
» ansible-vault edit data_vars/credentials.yml
Vault password: # Enter password contains in .ansible_vault_pass.txt
```

To create an encrypted file

```shell
» ansible-vault create data_vars/your_file.yml
New Vault password: # Enter your secure password
```

## Alerting

This module is not implemented for the moment


## References

#### sFlow

* https://grafana.com/grafana/dashboards/11201
* https://blog.sflow.com/2019/10/flow-metrics-with-prometheus-and-grafana.html
* https://blog.sflow.com/2019/04/prometheus-exporter.html
* https://grafana.com/grafana/dashboards/11146
* https://grafana.com/grafana/dashboards?search=sflow
* https://sflow.org/sFlowOverview.pdf


## Annexes


```python
docker run -d -p 8008:8008 -p 6343:6343/udp sflow/prometheus
```

http://<IP_ADDRESS>:8008/app/prometheus/api/index.html#/

